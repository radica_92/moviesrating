const express = require("express");
const router = express.Router();

const Movie = require("../../models/Movie");

//@route GET api/movies
//@desc get movies from db
//@access Public
router.get("/", (req, res) => {
  Movie.find()
    .sort({ rating: "desc" })
    .then(movies => res.json(movies));
});

module.exports = router;
