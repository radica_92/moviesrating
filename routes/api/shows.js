const express = require("express");
const router = express.Router();

const Show = require("../../models/Show");

//@route GET api/shows
//@desc get shows from db
//@access Public
router.get("/", (req, res) => {
  Show.find()
    .sort({ rating: "desc" })
    .then(shows => res.json(shows));
});

module.exports = router;
