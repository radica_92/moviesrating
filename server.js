const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const Movie = require("./models/Movie");
const Show = require("./models/Show");
const passport = require("passport");

const users = require("./routes/api/users");
const movies = require("./routes/api/movies");
const shows = require("./routes/api/shows");

const app = express();
//body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const db = require("./config/keys").mongoURI;
//connect to mongo
mongoose
  .connect(db)
  .then(() => {
    console.log("Mongo connected...");
  })
  .catch(err => console.log(err));

/*const movie = new Movie({
  title: "Life Is Beautiful",
  poster_path:
    "https://image.tmdb.org/t/p/w600_and_h900_bestv2/f7DImXDebOs148U4uPjI61iDvaK.jpg",
  release_date: "1997-12-20",
  rating: 5,
  description:
    "A touching story of an Italian book seller of Jewish ancestry who lives in his own little fairy tale. His creative and happy life would come to an abrupt halt when his entire family is deported to a concentration camp during World War II. While locked up he tries to convince his son that the whole thing is just a game.",
  cast: ["Nicoletta Braschi", "Roberto Benigni"]
});
movie.save();
*/
/*const show = new Show({
  title: "Peaky Blinders",
  poster_path:
    "https://image.tmdb.org/t/p/w600_and_h900_bestv2/jeWoeUQyHdxGFNZCrzbOUP78FiZ.jpg",
  release_date: "2013-09-12",
  rating: 4.5,
  description:
    "A gangster family epic set in 1919 Birmingham, England and centered on a gang who sew razor blades in the peaks of their caps, and their fierce boss Tommy Shelby, who means to move up in the world.",
  cast: ["Cillian Murphy", "Hellen Mcrory"]
});
show.save();
*/
//////////////////////////
// Passport middleware
app.use(passport.initialize());

require("./config/passport")(passport);

app.use("/api/movies", movies);
app.use("/api/shows", shows);
app.use("/api/users", users);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`server started on port ${port}`));
